import { Gitlab } from 'gitlab';
import errorCodes from './../../templates/errorCodes.json'

exports.default = async function setGroupRights({db, user, userId, userEmail, groupId, permission}) {
  const services = new Gitlab({
    oauthToken: user.gitlab.accessToken
  })

  const groups = await services.Groups.all({onwed: true})

  if(!groups.some(group => group.id === groupId))
    return ['error', errorCodes.groupo]

  const targetUser = await db.collection('users').findOne({ _id: userId, login: userEmail });

  if(!targetUser)
    return ['error', errorCodes.nosuchu]

  if(targetUser.permission.groups[groupId] === permission)
    return ['error', errorCodes.groupu]

  if(!targetUser.permission)
    targetUser.permission = { groups }

  if(!targetUser.permission.groups)
    targetUser.permission.groups = { }

  targetUser.permission.groups[groupId] = permission;

  await targetUser.save();

  return ['rights-ok']
}