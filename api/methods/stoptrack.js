exports.default = async function stoptrack({db, user, activity}) {
  await db.collection('time').updateOne({_id: activity._id}, { $set: activity });
  return [ 'stopped' ];
}