import { Gitlab } from 'gitlab';

exports.default = async function getgroups({db, user, groupId}) {
  const services = new Gitlab({
    oauthToken: user.gitlab.accessToken
  })

  const group = await services.Groups.show(groupId)
  
  return ['group', group]
}