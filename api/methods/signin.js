import jwt from 'jsonwebtoken'
import moment from 'moment-timezone'
import sha256 from 'sha256'

exports.default = async function signin({login, password, db, user, token, ref_token, syncTime}) {
  let _user = (await db.collection('users').find({ email: login }).toArray())[0]
  
  if(!_user)
    return ['signin-fail', 'No such user']
  
  const userPrep = async (isOk, reason) => {
    user = _user;
    user.tz = moment.parseZone(syncTime).format('Z');
    await db.collection('users').updateOne( { _id: _user._id }, { $set: user } );
    delete user.password;
    return [isOk ? 'signin-ok' : 'signin-fail', reason || user]
  }

  if(token)
    if(_user.access_token == token)
    {
      const accessTokenExpiredAndRefreshTokenNot = moment().diff(jwt.decode(_user.access_token).authed, 'hours') >= 24 && _user.refresh_token == ref_token;
      const refreshTokenExpiredOrDamaged = !_user.refresh_token || moment().diff(jwt.decode(_user.refresh_token), 'hours') >= 24*14;
      if(accessTokenExpiredAndRefreshTokenNot)
      {
        _user.access_token = jwt.sign({ id: _user._id, authed: moment().toISOString() }, 't-RAcker' )
        _user.refresh_token = jwt.sign(moment().toISOString(), 't-RAcker')
        return await userPrep(true)
      }
      else if(refreshTokenExpiredOrDamaged)
      {
        _user.access_token = null;
        _user.refresh_token = null;
        return await userPrep(false, 'Token expired or damaged');
      }
      return await userPrep(true)
    }
    else if(_user.refresh_token == ref_token)
    {
      _user.access_token = jwt.sign({ id: _user._id, authed: moment().toISOString() }, 't-RAcker' )
      _user.refresh_token = jwt.sign(moment().toISOString(), 't-RAcker' )
      
      return await userPrep(true);
    }
    else
    {
      delete _user.access_token;
      delete _user.refresh_token;
    }

  const passwordHash = password ? sha256(sha256(password, { asString: true }), { asString: true }) : null
  
  if(_user.password != passwordHash)
    return ['signin-fail', 'Wrong password']

  if(!_user.access_token || !_user.refresh_token)
  {
    _user.access_token = jwt.sign({ id: _user._id, authed: moment().toISOString() }, 't-RAcker' )
    _user.refresh_token = jwt.sign(moment().toISOString(), 't-RAcker' )
  }

  return await userPrep(true)
}