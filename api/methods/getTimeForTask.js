import moment from 'moment'
import toLocalDate from '../../utils/toLocalDate';

exports.default = async function getoverall({db, user, projectId, taskId}) {
  const todayActivities = await db.collection('time').find(
    { 
      userId: user._id, 
      date: toLocalDate(moment(), user).format('MM/DD/YYYY') 
    }).toArray();
  const overall = todayActivities
    .filter(act => act.taskId === taskId && act.projectId === projectId)
    .map(act => act.tracked)
    .reduce((a,b) => a + b, 0);
  return [ 'time', overall ]
}