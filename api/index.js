var signin = require('./methods/signin').default;
var signup = require('./methods/signup').default;
var logout = require('./methods/logout').default;
var getuser = require('./methods/getuser').default;
var getoverall = require('./methods/getoverall').default;
var saveuser = require('./methods/saveuser').default;
var starttrack = require('./methods/starttrack').default;
var stoptrack = require('./methods/stoptrack').default;
var track = require('./methods/track').default;
var logouteverywhere = require('./methods/logouteverywhere').default;
var auth_gitlab = require('./methods/auth_gitlab').default;
var getprojects = require('./methods/getprojects').default;
var gettasks = require('./methods/gettasks').default;
var getTimeForTask = require('./methods/getTimeForTask').default;
var getgroups = require('./methods/getgroups').default;
var getgroup = require('./methods/getgroup').default;
var setGroupRights = require('./methods/setGroupRights').default;

exports.default = { 
  signin, 
  signup,
  logout, 
  getuser, 
  getoverall, 
  saveuser, 
  starttrack, 
  stoptrack, 
  track, 
  logouteverywhere,
  auth_gitlab,
  getprojects,
  gettasks,
  getTimeForTask,
  getgroups,
  getgroup,
  setGroupRights
}