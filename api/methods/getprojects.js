import { Gitlab } from 'gitlab'

exports.default = async function getprojects({db, user}) {
  const services = new Gitlab({
    oauthToken: user.gitlab.accessToken
  })

  const projects = await services.Projects.all({ minAccessLevel: 30 })

  return ['projects', projects]
}