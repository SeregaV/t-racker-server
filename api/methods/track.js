exports.default = async function track({db, user, activity}) {
  if(!activity)
    return ['track-error', 'activity lost'];
  
  activity.tracked += 1;
  activity.overall += 1;

  return ['activity', activity];
}