import { Gitlab } from 'gitlab';

exports.default = async function gettasks({db, user, projectId}) {
  const services = new Gitlab({
    oauthToken: user.gitlab.accessToken
  })

  const tasks = await services.Issues.all({projectId, state: 'opened', assignee_id: user.gitlab.id})

  return ['tasks', tasks]
}