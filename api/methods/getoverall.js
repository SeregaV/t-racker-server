import moment from 'moment-timezone'
import toLocalDate from '../../utils/toLocalDate';

const enumDays = (user, today) => {
  let start = moment(today).startOf('iweek').isoWeekday(1);
  const days = [];

  while(!(start.dayOfYear() > today.dayOfYear())){
    days.push(start.format('MM/DD/YYYY'))
    start = start.add(1, 'days')
  }
  return days;
}

exports.default = async function getoverall({db, user, forWeek}) {
  let activities;
  const today = toLocalDate(moment(), user);
  if(forWeek){
    const weekDaysGone = enumDays(user, today)
    activities = await db.collection('time').find({
      userId: user._id,
      date: {
        $in: weekDaysGone
      }
    }).toArray()
  }
  else{
    activities = await db.collection('time').find({ userId: user._id, date: today.format('MM/DD/YYYY') }).toArray();
  }
  const overall = activities.map(act => act.tracked).reduce((a,b) => a + b, 0);
  return [ 'overall', overall ]
}