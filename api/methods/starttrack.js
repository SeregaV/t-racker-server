import moment from 'moment'

exports.default = async function starttrack({db, user, projectId, taskId}) {
  let overall;
  if(taskId && projectId){
    const todayActivities = await db.collection('time').find({ userId: user._id, date: moment().format('MM/DD/YYYY') }).toArray();
    overall = todayActivities.filter(act => act.taskId === taskId && act.projectId === projectId).map(act => act.tracked).reduce((a,b) => a + b, 0);
  }else{
    const todayActivities = await db.collection('time').find({ userId: user._id, date: moment().format('MM/DD/YYYY') }).toArray();
    overall = todayActivities.filter(act => act.taskId === -1 && (act.projectId === projectId || act.projectId === -1)).map(act => act.tracked).reduce((a,b) => a + b, 0);
  }
  
  const date = moment.tz(moment.utc(), user.tz).format('MM/DD/YYYY');
  const start = moment.tz(moment.utc(), user.tz).toISOString();

  let activity = await db.collection('time').insertOne({
    userId: user._id,
    date,
    start,
    projectId: projectId || -1,
    taskId: taskId || -1,
    tracked: 0,
    overall,
  })

  return ['activity', activity.ops[0]];
}