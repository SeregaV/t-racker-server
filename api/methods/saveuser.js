import { ObjectId } from 'mongodb'

exports.default = async function saveuser({id, db, user}) {
  const uid = user._id || id
  delete user._id
  var res = await db.collection('users').updateOne( { _id: ObjectId(uid) }, { $set: user } );
  let resp = 'upd-fail'
  if(res.modifiedCount === 1)
    resp = 'upd-ok'
  else if(res.modifiedCount === 0 && res.matchedCount === 1)
    resp = 'upd-none'
  return [ resp ]
}