import jwt from 'jsonwebtoken'
import moment from 'moment'
import sha256 from 'sha256'

exports.default = async function signin({login, password, db, company }) {
  let _user = (await db.collection('users').find({ email: login }).toArray())[0]
  
  if(_user)
    return ['signup-fail', 'User already exists!']
  
  const passwordHash = sha256(sha256(password, { asString: true }), { asString: true });
  _user = {
    email: login,
    password: passwordHash,
    companies: [ company ]
  }

  if(!_user.access_token || !_user.refresh_token)
  {
    _user.access_token = jwt.sign({ id: _user._id, authed: moment().toISOString() }, 't-RAcker' )
    _user.refresh_token = jwt.sign(moment().toISOString(), 't-RAcker' )
  }
  
 
  await db.collection('users').insertOne(_user).catch(console.error);
  delete _user.password;
  return ['signin-ok', _user]
}