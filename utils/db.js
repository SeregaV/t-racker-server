import dotenv from 'dotenv'
import { MongoClient } from 'mongodb'

process.env = { ...process.env, ...dotenv.config().parsed }

const uri = `mongodb+srv://${process.env.atlasuser}:${process.env.atlaspass}@cluster0-zvexx.mongodb.net/t-RAcker?retryWrites=true&w=majority&appName=${process.env.atlasAppId}:mongodb-atlas:api-key`;

export default new MongoClient(uri, { 
  useNewUrlParser: true
 }); 
