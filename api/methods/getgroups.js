import { Gitlab } from 'gitlab';

exports.default = async function getgroups({db, user}) {
  const services = new Gitlab({
    oauthToken: user.gitlab.accessToken,
  })

  const ownedGroups = await services.Groups.all({ min_access_level: 50 })
  const allGroups = await services.Groups.all()
  
  if(!user.permissions)
  {
    user.permissions = {
      groups: [ ...ownedGroups.map(group => group.id) ]
    }
  }
  if(!user.permissions.groups)
  {
    user.permissions.groups = [ ...ownedGroups.map(group => group.id) ]
  }
  await db.collection('users').updateOne( { _id: user._id }, { $set: user } );
  
  const filteredGroups = [
    ...ownedGroups,
    ...allGroups.filter(group => user.permissions.groups[group.id]),
  ]
  return ['groups', filteredGroups]
}