import { ObjectId } from 'mongodb'

exports.default = async function logout({db, user}) {
  await db.collection('users').updateOne({_id: ObjectId(user._id) }, {$set: { access_token: null, refresh_token: null }});
  return ['loggedout']
}