import { Strategy as GitLabStrategy } from 'passport-gitlab2'
import passport from 'passport'

exports.default = function auth_gitlab({ db, userId, state, user, from_gitlab }) {
  const callbackURL = `https://t-racker.herokuapp.com/auth/gitlab/callback`;
  passport.use(new GitLabStrategy({
      clientID: process.env.gitlabAppId,
      clientSecret: process.env.gitlabAppSecret,
      callbackURL,
    },
    function(accessToken, refreshToken, profile, cb) {
      console.log('I`m here!')
      cb({accessToken, refreshToken, profile, userId: userId || state});
    }
  ));

  if(from_gitlab)
  {
    return passport.authenticate('gitlab', {
      scope: ['api'],
      failureRedirect: '/',
      state:  user ? user._id : ( userId || state )
    });
  }
  else
  {
    return ['gitlab_auth_next', `https://t-racker.herokuapp.com/auth/gitlab?userId=${user._id}` ]  
  }
}