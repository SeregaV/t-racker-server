import { ObjectId } from 'mongodb'

exports.default = async function getuser({db, user}) {
  const r_user = await db.collection('users').findOne({ _id: ObjectId(user._id) })
  delete r_user.password
  return ['user', r_user];
}