var express = require('express');
var app = express();
var http = require('http').Server(app);
var srv = require('socket.io')(http);

import { ObjectId } from 'mongodb'
import dotenv from 'dotenv'
process.env = { ...process.env, ...dotenv.config().parsed }


var methods = require('./api/index').default;
var client = require('./utils/db').default;


async function run () {
  const db = (await client.connect()).db('t-RAcker');

  app.get('/', function(req, res){
    res.sendFile(__dirname+'/templates/404.html');
  });

  app.get('/auth/gitlab', (req, res) => methods.auth_gitlab({db, from_gitlab: true, userId: req.query.state || req.query.userId })(req, res), function(req, res){
    res.sendFile(__dirname+'/templates/successful_login.html')
  });

  app.get('/auth/gitlab/callback', 
  (req, res) => methods.auth_gitlab({from_gitlab: true, db, userId: req.query.state || req.query.userId})(req, res, async ({accessToken, refreshToken, profile, userId }, arg) => {
    console.log('And here!');
    const uid = ObjectId(userId || state);
    console.log(uid);
    const gitlab_object = { accessToken, refreshToken, id: profile.id }
    console.log(gitlab_object);
    const user = await db.collection('users').findOne({ _id: uid });
    console.log(user)
    await db.collection('users').updateOne( { _id: uid }, 
        { "$set": 
          { 
            "gitlab": gitlab_object 
          } 
        }).catch(console.error)
    console.log('And already done..!');
    res.sendFile(__dirname+'/templates/successful_login.html');
    console.log('And return..!');
  }));

  console.log(methods);

  srv.on('error', (reason) => {
    console.error('SRV Error', reason);
  })
  srv.on('connect_error', function(err) {
    console.error('SRV Error', err);
  });


  srv.on('connection', (socket) => {
    console.log('..new connection')
    let user, activity;

    socket.on('error', (reason) => {
      console.error('Socket Error', reason);
    })
    socket.on('connect_error', function(err) {
      console.error('Socket Error', err);
    });

    socket.on('disconnect', async (reason) => {
      console.error('Socket Disconnect', reason);
      if(user)
        await db.collection('users').updateOne( { _id: user._id }, { $set: user } );

      if(activity)
        await db.collection('time').updateOne({_id: activity._id}, { $set: activity });
    })

    socket.on('action', async (payload, cb) => {
      // console.log('before', activity);
      payload = { ...payload, db, id: user ? user._id : undefined, user: payload.user ? payload.user : user, activity };
      if(payload.method != "track")
        console.log(payload.method)
      let result = await methods[payload.method](payload);
      // console.log(result);
      
      if(result[0] == 'signin-ok' || result[0] == 'user')
        user = result[1];

      if(result[0] == 'activity')
        activity = result[1];

      if(result[0] == 'stopped')
        activity = null;

      if(cb)
        cb(result);
      else
        socket.emit(...result);

      // console.log('after', activity);
    })
  });

  http.listen(process.env.PORT || 80, function(){
    console.log(`listening on *:${process.env.PORT}`);
  });
}
run()